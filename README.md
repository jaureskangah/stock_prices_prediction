Set up Local env

pipenv install --python 3.6
pipenv shell
jupyter contrib nbextension install --sys-prefix
jupyter nbextension enable codefolding/main
jupyter nbextension enable execute_time/ExecuteTime
jupyter nbextension enable toc2/main
jupyter nbextension enable hide_input_all/main